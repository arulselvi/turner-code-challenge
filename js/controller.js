// Define a new module for our app
var app = angular.module("instantSearch", []);

app.filter('offset', function() {
	  return function(input, start) {
			start = parseInt(start, 10);
			return input.slice(start);
		};
	});
	


// The controller


function InstantSearchController($scope,$http){
		delete $http.defaults.headers.common['X-Requested-With'];
		var temp_array = new Array();
		$scope.fav = {};
		$scope.items = {};
		
		$scope.search = function(search_type){
		if($scope.searchString!='')
		$scope.title = "Artist-" +$scope.searchString;
		else
		{
		$scope.title = '';
		$scope.albumtitle = '';
		$scope.tracktitle = '';
		}
		
		var url="https://api.spotify.com/v1/search";
		var responsePromise = $http.get( url,
             {  params : {
                   q : $scope.searchString
                  ,type : 'artist'
                }
              }
            );

            responsePromise.success(function(data, status, headers, config) {
					$scope.items.fromServer = data.artists.items;
					
					if(data.artists.items.length>6)
						$scope.page_flag="show";
					else
						$scope.page_flag="hide";
					
					$scope.album_flag = "hide";
					$scope.artist_flag = "show";
					$scope.track_flag = "hide";
                });
                responsePromise.error(function(data, status, headers, config) {
                    console.log("AJAX failed!");
                });
        }    

$scope.show_album_artists = function(album_id,album_name,view_type){

var text;
if(view_type=='albums')
{
   text = "artists";
   $scope.albumtitle="/ Album-" +">"+album_name;
}
else{
	text = "albums";
	$scope.tracktitle="/ Track-" +">"+album_name;
}

	
var url='https://api.spotify.com/v1/'+text+'/'+album_id+'/'+view_type;
var responsePromise = $http.get(url);

                 responsePromise.success(function(data, status, headers, config) {
				 if(view_type=='albums'){
                    $scope.albums = data.items;
					if(data.items.length>6)
					$scope.page_flag="show";
					else
					$scope.page_flag="hide";
					$scope.album_flag = "show";
					$scope.artist_flag = "hide";
					$scope.track_flag = "hide";
					
					

					
					}
					else
					{
					$scope.tracks = data.items;
					$scope.page_flag="hide";
					
					$scope.track_flag = "show";
					$scope.artist_flag = "hide";
					$scope.album_flag = "hide";
					
					
					}
					
					
                });
                responsePromise.error(function(data, status, headers, config) {
                    console.log("AJAX failed!");
                });

};	

$scope.jumpto = function(type){

if(type=='artists'){
	$scope.track_flag = "hide";
	$scope.artist_flag = "show";
	$scope.album_flag = "hide";
	}
if(type=='albums'){
	$scope.track_flag = "hide";
	$scope.artist_flag = "hide";
	$scope.album_flag = "show";
	}
if(type=='tracks'){
	$scope.track_flag = "show";
	$scope.artist_flag = "hide";
	$scope.album_flag = "hide";
	}

}
$scope.savetofav = function(k_val){
	
	temp_array.push(k_val);
	$scope.fav = temp_array;
	console.log(temp_array);
	
}

$scope.deletefav = function(index){
	$scope.fav.splice(index, 1);
}              


	

$scope.itemsPerPage = 6;
  $scope.currentPage = 0;

 
 
}
